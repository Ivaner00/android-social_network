package ru.ivan.eremin.socialnetwork.models.response.user

import kotlinx.serialization.Serializable

@Serializable
data class AuthResponse(
    val token: String? = null,
    val userId: String? = null,
) : BaseResponse()