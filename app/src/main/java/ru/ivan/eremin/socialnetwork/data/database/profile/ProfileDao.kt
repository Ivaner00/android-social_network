package ru.ivan.eremin.socialnetwork.data.database.profile

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

@Dao
interface ProfileDao {
    @Query("SELECT * FROM profile WHERE profile_id LiKE :userId LIMIT 1")
    suspend fun getProfileById(userId: String): ProfileEntity?

    @Insert(onConflict = REPLACE)
    suspend fun insert(profile: ProfileEntity)

    @Query("DELETE FROM profile")
    suspend fun clearAll()

    @Delete
    suspend fun delete(profile: ProfileEntity)
}