package ru.ivan.eremin.socialnetwork.models

data class Comment (
    val commentId: Int = -1,
    val username: String = "",
    val profilePicture: String = "",
    val timestamp: Long = System.currentTimeMillis(),
    val comment: String = "",
    val isLiked: Boolean = false,
    val likeCount: Int = 12
)