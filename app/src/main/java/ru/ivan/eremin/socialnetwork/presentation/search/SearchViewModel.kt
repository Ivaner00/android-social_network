package ru.ivan.eremin.socialnetwork.presentation.search

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import ru.ivan.eremin.socialnetwork.models.user.User
import ru.ivan.eremin.socialnetwork.presentation.util.states.StandardTextFieldState
import javax.inject.Inject

@HiltViewModel
class SearchViewModel  @Inject constructor(

) : ViewModel(){
    private val _searchState = mutableStateOf(StandardTextFieldState())
    val searchState: State<StandardTextFieldState> = _searchState

    private val _usersState = mutableStateOf(arrayListOf<User>())
    val usersState: State<List<User>> = _usersState
    fun setSearchState(state: StandardTextFieldState){
        _searchState.value = state
    }
}