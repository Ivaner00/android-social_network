package ru.ivan.eremin.socialnetwork.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.ivan.eremin.socialnetwork.data.repository.user.UserRepositoryImpl
import ru.ivan.eremin.socialnetwork.domain.provider.user.UserRepository


@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {
    @Binds
    fun bindUserRepository(
        repository: UserRepositoryImpl
    ): UserRepository

}