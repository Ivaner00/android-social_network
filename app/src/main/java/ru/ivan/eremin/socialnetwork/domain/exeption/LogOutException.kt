package ru.ivan.eremin.socialnetwork.domain.exeption

sealed class InternalException: Exception() {
    object LogOutException : InternalException()
    object NetworkConnectionException : InternalException()
    object NullPointCache : InternalException()
}