package ru.ivan.eremin.socialnetwork.domain.provider.user

import ru.ivan.eremin.socialnetwork.models.post.Post
import ru.ivan.eremin.socialnetwork.models.response.user.BaseResponse
import ru.ivan.eremin.socialnetwork.models.user.User

interface UserProvider {
    suspend fun createUser(email: String, username: String, password: String): Result<BaseResponse>
    suspend fun loginUser(email: String, password: String): Result<BaseResponse>
    suspend fun getUserProfile(): Result<User>
    suspend fun checkAuth(): Result<Boolean>
    suspend fun getPostsForProfile(): Result<List<Post>>
}