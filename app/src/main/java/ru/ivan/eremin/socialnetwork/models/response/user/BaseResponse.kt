package ru.ivan.eremin.socialnetwork.models.response.user


open class BaseResponse {
    var successful: Boolean? = true
    var message: String? = ""
}