package ru.ivan.eremin.socialnetwork.models

import ru.ivan.eremin.socialnetwork.domain.util.ActivityAction

data class Activity(
    val username: String,
    val actionType: ActivityAction,
    val formattedTime: String
)
