package ru.ivan.eremin.socialnetwork.models.request.user

import kotlinx.serialization.Serializable

@Serializable
data class LoginRequest (val email: String, val password: String)