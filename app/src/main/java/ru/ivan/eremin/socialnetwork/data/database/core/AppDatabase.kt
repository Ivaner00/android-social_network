package ru.ivan.eremin.socialnetwork.data.database.core

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ru.ivan.eremin.socialnetwork.data.database.post.PostDao
import ru.ivan.eremin.socialnetwork.data.database.post.PostEntity
import ru.ivan.eremin.socialnetwork.data.database.profile.ProfileDao
import ru.ivan.eremin.socialnetwork.data.database.profile.ProfileEntity

@Database(entities = [ProfileEntity::class, PostEntity::class], version = 1)
@TypeConverters(StringListConverters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun profileDao(): ProfileDao
    abstract fun postDao(): PostDao

    companion object {
        private var INSTANCE: AppDatabase? = null
        fun dataBaseBuild(context: Context): AppDatabase {
           if(INSTANCE == null){
               synchronized(AppDatabase::class){
                   INSTANCE = Room.databaseBuilder(
                       context.applicationContext,
                       AppDatabase::class.java,
                       "social_network.db"
                   ).build()
               }
           }
            return INSTANCE!!
        }
    }
}
