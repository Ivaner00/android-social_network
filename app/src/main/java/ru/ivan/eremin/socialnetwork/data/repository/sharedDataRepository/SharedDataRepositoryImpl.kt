package ru.ivan.eremin.socialnetwork.data.repository.sharedDataRepository

import android.content.SharedPreferences
import javax.inject.Inject


class SharedDataRepositoryImpl @Inject constructor(
    private val sharedPreferences: SharedPreferences
) : SharedDataRepository {

    companion object {
        private val ACCESS_TOKEN = "access_token"
        private val USER_ID = "user_id"
    }

    override fun saveAccessToken(token: String) {
        sharedPreferences
            .edit().apply {
                putString(ACCESS_TOKEN, token)
            }.apply()
    }

    override fun getAccessToken(): String {
        return sharedPreferences
            .getString(ACCESS_TOKEN, "") ?: ""
    }

    override fun clearAccessToken() {
        sharedPreferences.edit()
            .apply {
                remove(ACCESS_TOKEN)
            }
            .apply()
    }

    override fun saveUserId(userId: String) {
        sharedPreferences
            .edit().apply {
                putString(USER_ID, userId)
            }.apply()
    }

    override fun getUserId(): String {
        return sharedPreferences
            .getString(USER_ID, "") ?: ""
    }

    override fun clearUserId() {
        sharedPreferences.edit()
            .apply {
                remove(USER_ID)
            }
            .apply()
    }
}