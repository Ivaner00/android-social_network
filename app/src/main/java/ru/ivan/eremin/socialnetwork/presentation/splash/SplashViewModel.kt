package ru.ivan.eremin.socialnetwork.presentation.splash

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import ru.ivan.eremin.socialnetwork.domain.provider.user.UserProvider
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val userProvider: UserProvider
) : ViewModel() {
    private val _authCheckState = mutableStateOf(SplashState())
    val authCheckState: State<SplashState> = _authCheckState

    init {
        viewModelScope.launch {
            userProvider.checkAuth()
                .onSuccess {
                    _authCheckState.value = _authCheckState.value.copy(
                        isCheckAuth = it
                    )
                }.onFailure {
                    _authCheckState.value = _authCheckState.value.copy(
                        errorMessage = it.localizedMessage!!
                    )
                }
        }
    }
}