package ru.ivan.eremin.socialnetwork.models.response.user

import kotlinx.serialization.Serializable

@Serializable
data class PostResponse(
    val postId: String,
    val username: String,
    val imageUrl: String,
    val profilePictureUrl: String,
    val description: String,
    val likeCount: Int,
    val commentCount: Int
) : BaseResponse()