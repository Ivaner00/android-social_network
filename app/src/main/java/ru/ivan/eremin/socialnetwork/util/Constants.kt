package ru.ivan.eremin.socialnetwork.util

object Constants {
    const val MIN_USERNAME_LENGTH = 3
    const val MIN_PASSWORD_LENGTH = 3

    const val SPLASH_SCREEN_DURATION = 0L

    const val MAX_POST_DESCRIPTION_LINES = 3
}