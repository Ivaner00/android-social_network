package ru.ivan.eremin.socialnetwork.data.repository.sharedDataRepository

interface SharedDataRepository {
    fun saveAccessToken(token: String)
    fun getAccessToken(): String
    fun clearAccessToken()

    fun saveUserId(userId: String)
    fun getUserId(): String
    fun clearUserId()

}