package ru.ivan.eremin.socialnetwork.models.response.user

import kotlinx.serialization.Serializable

@Serializable
data class RegisterResponse(
    val userId: String? = null
) : BaseResponse()