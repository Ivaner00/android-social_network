package ru.ivan.eremin.socialnetwork.data.database.post

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query

@Dao
interface PostDao {
    @Query("SELECT * FROM post WHERE user_id LIKE :userId")
    suspend fun getPostsForProfile(userId: String): List<PostEntity>?

    @Insert(onConflict = REPLACE)
    suspend fun insert(posts: List<PostEntity>)
}