package ru.ivan.eremin.socialnetwork.data.httpclient

import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.http.*
import ru.ivan.eremin.socialnetwork.data.repository.sharedDataRepository.SharedDataRepository
import java.net.ConnectException
import javax.inject.Inject

class KtorHttpClientImpl @Inject constructor(
    private val sharedData: SharedDataRepository
) : KtorHttpClient {
    override fun createHttpClient(): HttpClient {
        return HttpClient(Android) {
            defaultRequest {
               // host = "192.168.0.116"
                // host = "192.168.43.4"
                host = "192.168.3.10"
                port = 8080
                url {
                    protocol = URLProtocol.HTTP
                }

                header(HttpHeaders.ContentType, "application/json")
                header(HttpHeaders.Authorization, "Bearer ${sharedData.getAccessToken()}")
            }
            install(Logging) {
                logger = Logger.DEFAULT
                level = LogLevel.ALL
            }
            install(JsonFeature) {
                serializer = KotlinxSerializer()
            }
            expectSuccess = true

        }
    }
}

suspend fun <T> HttpClient.requestAndCatch(
    block: suspend HttpClient.() -> T,
    errorHandler: suspend Exception.() -> T
): T = runCatching { block() }
    .getOrElse {
        when (it) {
            is ResponseException -> it.errorHandler()
            is ConnectException -> it.errorHandler()
            else -> throw it
        }
    }