package ru.ivan.eremin.socialnetwork.data.database.core

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class StringListConverters {
    @TypeConverter
    fun fromSkills(value: String): List<String> {
        return Gson().fromJson(value, object : TypeToken<String>() {}.type)
    }

    @TypeConverter
    fun toSkills(value: List<String>): String {
        return Gson().toJson(value)
    }
}