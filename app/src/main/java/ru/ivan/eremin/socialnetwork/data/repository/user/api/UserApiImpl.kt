package ru.ivan.eremin.socialnetwork.data.repository.user.api

import io.ktor.client.*
import io.ktor.client.request.*
import ru.ivan.eremin.socialnetwork.data.httpclient.requestAndCatch
import ru.ivan.eremin.socialnetwork.data.repository.user.UserApi
import ru.ivan.eremin.socialnetwork.models.request.user.LoginRequest
import ru.ivan.eremin.socialnetwork.models.request.user.UserAccountRequest
import ru.ivan.eremin.socialnetwork.models.response.user.*
import javax.inject.Inject


class UserApiImpl @Inject constructor(
    private val client: HttpClient,
) : UserApi {
    override suspend fun createUser(user: UserAccountRequest): Result<RegisterResponse> {
        return client.requestAndCatch({
            Result.success(post("/api/user/create") {
                body = user
            })
        }, {
            Result.failure(this)
        })

    }

    override suspend fun loginUser(login: LoginRequest): Result<AuthResponse> {
        return client.requestAndCatch({
            Result.success(post("/api/user/login") {
                body = login
            })
        }, {
            Result.failure(this)
        })
    }

    override suspend fun checkAuth(userId: String): Result<CheckAuthResponse> {
        return client.requestAndCatch({
            Result.success(get("/api/user/checkAuth") {
                parameter("userId", userId)
            })
        }, {
            Result.failure(this)
        })
    }

    override suspend fun getUserProfile(userId: String): Result<ProfileResponse> {
        return client.requestAndCatch({
            Result.success(get("/api/user/profile") {
                parameter("userId", userId)
            })
        }, {
            Result.failure(this)
        })
    }

    override suspend fun getPostsForProfile(userId: String): Result<List<PostResponse>> {
        return client.requestAndCatch({
            Result.success(get("/api/user/posts") {
                parameter("userId", userId)
            })
        }, {
            Result.failure(this)
        })
    }
}