package ru.ivan.eremin.socialnetwork.data.httpclient

import io.ktor.client.*

interface KtorHttpClient {
    fun createHttpClient():HttpClient
}