package ru.ivan.eremin.socialnetwork.di

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.ktor.client.*
import ru.ivan.eremin.socialnetwork.data.database.core.AppDatabase
import ru.ivan.eremin.socialnetwork.data.database.post.PostDao
import ru.ivan.eremin.socialnetwork.data.database.profile.ProfileDao
import ru.ivan.eremin.socialnetwork.data.httpclient.KtorHttpClientImpl
import ru.ivan.eremin.socialnetwork.data.httpclient.NetworkHandler
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun bindKtorClient(
        ktor: KtorHttpClientImpl
    ): HttpClient{
        return ktor.createHttpClient()
    }

    @Provides
    @Singleton
    fun provideDataBase(
        @ApplicationContext context: Context
    ): AppDatabase {
        return AppDatabase.dataBaseBuild(context)
    }

    @Provides
    @Singleton
    fun provideProfileDao(
        database: AppDatabase
    ): ProfileDao {
        return database.profileDao()
    }

    @Provides
    @Singleton
    fun providePostDao(
        database: AppDatabase
    ): PostDao {
        return database.postDao()
    }

    @Provides
    @Singleton
    fun provideSharedPreference(
        @ApplicationContext context: Context
    ): SharedPreferences {
        return context.getSharedPreferences("default", Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideNetworkHandler(
        @ApplicationContext context: Context
    ): NetworkHandler {
        return NetworkHandler(context)
    }
}