package ru.ivan.eremin.socialnetwork.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.ivan.eremin.socialnetwork.domain.provider.user.UserProvider
import ru.ivan.eremin.socialnetwork.domain.provider.user.UserProviderImpl

@Module
@InstallIn(SingletonComponent::class)
interface ProviderModule {
    @Binds
    fun bindUserProvider(
        provider: UserProviderImpl
    ): UserProvider
}