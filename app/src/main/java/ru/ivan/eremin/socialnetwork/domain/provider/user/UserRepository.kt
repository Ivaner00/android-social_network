package ru.ivan.eremin.socialnetwork.domain.provider.user

import ru.ivan.eremin.socialnetwork.models.request.user.LoginRequest
import ru.ivan.eremin.socialnetwork.models.request.user.UserAccountRequest
import ru.ivan.eremin.socialnetwork.models.response.user.BaseResponse
import ru.ivan.eremin.socialnetwork.models.response.user.PostResponse
import ru.ivan.eremin.socialnetwork.models.response.user.ProfileResponse

interface UserRepository {
   suspend fun createUser(user: UserAccountRequest): Result<BaseResponse>
   suspend fun loginUser(login: LoginRequest):Result<BaseResponse>
   suspend fun getUserProfile(): Result<ProfileResponse>
    suspend fun checkAuth(): Result<Boolean>
    suspend fun getPostsForProfile(): Result<List<PostResponse>>
}