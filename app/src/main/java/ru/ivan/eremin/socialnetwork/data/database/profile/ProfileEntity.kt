package ru.ivan.eremin.socialnetwork.data.database.profile

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "profile")
data class ProfileEntity(
    @PrimaryKey
    @ColumnInfo(name = "profile_id")
    val userId: String,
    @ColumnInfo(name = "user_name")
    val username: String,
    val bio: String,
    @ColumnInfo(name = "follower_count")
    val followerCount: Int,
    @ColumnInfo(name = "following_count")
    val followingCount: Int,
    @ColumnInfo(name = "post_count")
    val postCount: Int,
    @ColumnInfo(name = "profile_picture_url")
    val profilePictureUrl: String,
    @ColumnInfo(name = "top_skill_urls")
    val topSkillUrls: List<String>,
    @ColumnInfo(name = "git_hub_url")
    val gitHubUrl: String?,
    @ColumnInfo(name = "instagram_url")
    val instagramUrl: String?,
    @ColumnInfo(name = "linked_in_url")
    val linkedInUrl: String?,
    @ColumnInfo(name = "is_own_profile")
    val isOwnProfile: Boolean,
    @ColumnInfo(name = "is_following")
    val isFollowing: Boolean,
    @ColumnInfo(name = "description")
    val description: String
)
