package ru.ivan.eremin.socialnetwork.data.repository.post.api

import io.ktor.client.*
import io.ktor.client.request.*
import ru.ivan.eremin.socialnetwork.data.httpclient.requestAndCatch
import ru.ivan.eremin.socialnetwork.data.repository.post.PostApi
import ru.ivan.eremin.socialnetwork.models.response.user.PostResponse
import javax.inject.Inject

class PostApiImpl @Inject constructor(
    private val client: HttpClient
) : PostApi {
    override suspend fun getPostsForProfile(): Result<List<PostResponse>> {
        return client.requestAndCatch({
            Result.success(post("/api/user/post"))
        }, {
            Result.failure(this)
        })
    }
}