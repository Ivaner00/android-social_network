package ru.ivan.eremin.socialnetwork.data.repository.post

import ru.ivan.eremin.socialnetwork.models.response.user.PostResponse

interface PostApi {
    suspend fun getPostsForProfile(): Result<List<PostResponse>>
}