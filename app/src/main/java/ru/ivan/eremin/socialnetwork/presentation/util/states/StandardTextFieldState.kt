package ru.ivan.eremin.socialnetwork.presentation.util.states

data class StandardTextFieldState(
    val text: String = "",
    val error: String = ""
)
