package ru.ivan.eremin.socialnetwork.domain.provider.user

import ru.ivan.eremin.socialnetwork.models.post.Post
import ru.ivan.eremin.socialnetwork.models.request.user.LoginRequest
import ru.ivan.eremin.socialnetwork.models.request.user.UserAccountRequest
import ru.ivan.eremin.socialnetwork.models.response.user.BaseResponse
import ru.ivan.eremin.socialnetwork.models.user.User
import javax.inject.Inject


class UserProviderImpl @Inject constructor (
    private val userRepository: UserRepository
): UserProvider {
    override suspend fun createUser(email: String, username: String, password: String) : Result<BaseResponse>{
       return userRepository.createUser(
            UserAccountRequest(
                email =  email,
                username = username,
                password = password
            )
        )
    }

    override suspend fun loginUser(email: String, password: String): Result<BaseResponse> {
        return userRepository.loginUser(
            LoginRequest(
                email = email,
                password = password
            )
        )
    }

    override suspend fun getUserProfile(): Result<User> {
        return userRepository.getUserProfile()
            .map {
                User(
                    userId = it.userId,
                    profilePictureUrl = it.profilePictureUrl,
                    username = it.username,
                    description = it.description,
                    followerCount = it.followerCount,
                    followingCount = it.followingCount,
                    postCount = it.postCount
                )
            }
    }

    override suspend fun checkAuth(): Result<Boolean> {
        return userRepository.checkAuth()
    }

    override suspend fun getPostsForProfile(): Result<List<Post>> {
        return userRepository.getPostsForProfile()
            .map {
                it.map {
                    Post(
                        postId = it.postId,
                        username = it.username,
                        imageUrl = it.imageUrl,
                        profilePictureUrl = it.profilePictureUrl,
                        description = it.description,
                        likeCount = it.likeCount,
                        commentCount = it.commentCount
                    )
                }
            }
    }
}

