package ru.ivan.eremin.socialnetwork.presentation.components

import android.view.animation.OvershootInterpolator
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationVector1D
import androidx.compose.animation.core.tween
import androidx.navigation.NavController
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import ru.ivan.eremin.socialnetwork.util.Constants

suspend fun RouteToAnimate(
    navController: NavController,
    scale: Animatable<Float, AnimationVector1D>,
    overshootInterpolator: OvershootInterpolator,
    route: String,
    dispatcher: CoroutineDispatcher = Dispatchers.Main
) {

    withContext(dispatcher) {
        scale.animateTo(
            targetValue = 0.5f,
            animationSpec = tween(
                durationMillis = 500,
                easing = {
                    overshootInterpolator.getInterpolation(it)
                }
            )
        )
        delay(Constants.SPLASH_SCREEN_DURATION)
        navController.popBackStack()
        navController.navigate(route)
    }
}