package ru.ivan.eremin.socialnetwork.presentation.login

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import ru.ivan.eremin.socialnetwork.domain.provider.user.UserProvider
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val userProvider: UserProvider
) : ViewModel() {

    private val _state = mutableStateOf(LoginState())
    val state: State<LoginState> = _state

    fun onEvent(event: LoginEvent){
        when(event){
            is LoginEvent.EnteredEmail->{
                _state.value = _state.value.copy(
                    emailText = event.value
                )
            }
            is LoginEvent.EnteredPassword ->{
                _state.value = _state.value.copy(
                    passwordText = event.value
                )
            }
            is LoginEvent.TogglePasswordVisibility->{
                _state.value = _state.value.copy(
                    isPasswordVisible = !state.value.isPasswordVisible
                )
            }

            is LoginEvent.Login->{
                if(!validateEmail(state.value.emailText)){
                    return
                }
                if(!validatePassword(state.value.passwordText)){
                    return
                }

                viewModelScope.launch {
                    userProvider.loginUser(
                        email = state.value.emailText,
                        password = state.value.passwordText
                    ).onSuccess {
                        _state.value = _state.value.copy(
                            loginResult = LoginState.LoginResult.Success(
                                it.successful ?: true,
                                it.message
                            )
                        )
                    }.onFailure {
                        _state.value = _state.value.copy(
                            loginResult = LoginState.LoginResult.Failed(
                                it.localizedMessage
                            )
                        )
                    }
                }
            }
        }
    }

    private fun validateEmail(email: String): Boolean{
        val trimmedEmail = email.trim()
        if(trimmedEmail.isBlank()){
            _state.value = _state.value.copy(
                emailError = LoginState.EmailError.FieldEmpty
            )
            return false
        }
        _state.value = _state.value.copy(emailError = null)
        return true
    }

    private fun validatePassword(password: String): Boolean{
        if(password.isBlank()){
            _state.value = _state.value.copy(
                passwordError = LoginState.PasswordError.FieldEmpty
            )

            return false
        }
        _state.value = _state.value.copy(passwordError = null)
        return true
    }
}