package ru.ivan.eremin.socialnetwork.presentation.login

data class LoginState (
    val emailText: String = "",
    val emailError: EmailError? = null,
    val passwordText: String = "",
    val passwordError: PasswordError? = null,
    val isPasswordVisible: Boolean = false,
    val loginResult: LoginResult?= null
){
    sealed class EmailError{
        object FieldEmpty: EmailError()
    }

    sealed class PasswordError{
        object FieldEmpty: PasswordError()
    }

    sealed class LoginResult{
        class Success(val successful: Boolean, val message: String?=null):LoginResult()
        class Failed(val message: String?=null):LoginResult()
    }
}