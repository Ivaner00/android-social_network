package ru.ivan.eremin.socialnetwork.models.request.user

import kotlinx.serialization.Serializable

@Serializable
data class UserAccountRequest(
    val email: String,
    val username: String,
    val password: String
)