package ru.ivan.eremin.socialnetwork.presentation.splash

import android.view.animation.OvershootInterpolator
import androidx.compose.animation.core.Animatable
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.ScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.res.painterResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.ivan.eremin.socialnetwork.R
import ru.ivan.eremin.socialnetwork.presentation.components.RouteToAnimate
import ru.ivan.eremin.socialnetwork.presentation.components.SnackBar
import ru.ivan.eremin.socialnetwork.presentation.util.Screen

@Composable
fun SplashScreen(
    navController: NavController,
    scaffoldState: ScaffoldState,
    dispatcher: CoroutineDispatcher = Dispatchers.Main,
    viewModel: SplashViewModel = hiltViewModel()
) {
    val scale = remember {
        Animatable(0f)
    }

    val overshootInterpolator = remember {
        OvershootInterpolator(2f)
    }
    val splashState = viewModel.authCheckState
    splashState.value.isCheckAuth?.let { isCheck ->
        LaunchedEffect(true) {
            withContext(dispatcher) {
                if (isCheck) {
                    RouteToAnimate(
                        navController = navController,
                        scale = scale,
                        overshootInterpolator = overshootInterpolator,
                        route = Screen.MainFeedScreen.route,

                        )
                } else {
                    RouteToAnimate(
                        navController = navController,
                        scale = scale,
                        overshootInterpolator = overshootInterpolator,
                        route = Screen.LoginScreen.route,
                    )
                }
            }
        }
    }
    if (splashState.value.errorMessage != null) {
        SnackBar(scaffoldState = scaffoldState, message = splashState.value.errorMessage ?: "")
        LaunchedEffect(splashState) {
            RouteToAnimate(
                navController = navController,
                scale = scale,
                overshootInterpolator = overshootInterpolator,
                route = Screen.LoginScreen.route,
            )
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_logo),
            contentDescription = "Logo",
            modifier = Modifier.scale(scale.value)
        )
    }
}