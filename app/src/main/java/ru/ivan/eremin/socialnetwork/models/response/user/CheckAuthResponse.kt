package ru.ivan.eremin.socialnetwork.models.response.user

import kotlinx.serialization.Serializable

@Serializable
data class CheckAuthResponse(
    val isCheck: Boolean? = null
) : BaseResponse()