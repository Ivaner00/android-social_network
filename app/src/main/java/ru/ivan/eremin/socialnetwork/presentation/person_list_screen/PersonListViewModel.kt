package ru.ivan.eremin.socialnetwork.presentation.person_list_screen

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import ru.ivan.eremin.socialnetwork.models.user.User
import javax.inject.Inject

@HiltViewModel
class PersonListViewModel  @Inject constructor(

): ViewModel(){
    private val _personsList = mutableStateOf(arrayListOf<User>())
    val personList: State<List<User>> = _personsList
}