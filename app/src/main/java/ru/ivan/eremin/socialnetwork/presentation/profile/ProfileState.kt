package ru.ivan.eremin.socialnetwork.presentation.profile

import ru.ivan.eremin.socialnetwork.models.post.Post
import ru.ivan.eremin.socialnetwork.models.user.User

data class ProfileState(
    val profile: User = User(),
    val exception: Throwable? = null,
    val posts: List<Post> = arrayListOf(),
)