package ru.ivan.eremin.socialnetwork.util

import java.util.concurrent.ConcurrentHashMap

object MemoryCache {
    val cache = ConcurrentHashMap<String, Any?>()

    fun getValue(key: String): Any? {
        return cache[key]
    }

    fun clearCache(key: String) {
        cache.remove(key)
    }

    fun clearAll() {
        cache.clear()
    }

    fun clearCacheContains(key: String) {
        cache.keys.filter {
            it.contains(key)
        }.forEach {
            cache.remove(it)
        }
    }

    fun exists(key: String): Boolean {
        return cache[key] != null
    }

    fun save(key: String, value: Any?) {
        cache[key] = value
    }
}

fun saveResult(key: String, value: Any): Any {
    MemoryCache.cache[key] = value
    return value
}

fun clear(key: String): Any? {
    val value = MemoryCache.cache[key]
    MemoryCache.clearCache(key)
    return value
}

