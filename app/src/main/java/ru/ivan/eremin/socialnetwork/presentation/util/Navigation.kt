package ru.ivan.eremin.socialnetwork.presentation.util

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import kotlinx.coroutines.CoroutineScope
import ru.ivan.eremin.socialnetwork.models.post.Post
import ru.ivan.eremin.socialnetwork.presentation.PersonListScreen
import ru.ivan.eremin.socialnetwork.presentation.activity.ActivityScreen
import ru.ivan.eremin.socialnetwork.presentation.chat.ChatScreen
import ru.ivan.eremin.socialnetwork.presentation.create_post.CreatePostScreen
import ru.ivan.eremin.socialnetwork.presentation.edit_profile.EditProfileScreen
import ru.ivan.eremin.socialnetwork.presentation.login.LoginScreen
import ru.ivan.eremin.socialnetwork.presentation.main_feed.MainFeedScreen
import ru.ivan.eremin.socialnetwork.presentation.post_detail.PostDetailScreen
import ru.ivan.eremin.socialnetwork.presentation.profile.ProfileScreen
import ru.ivan.eremin.socialnetwork.presentation.register.RegisterScreen
import ru.ivan.eremin.socialnetwork.presentation.search.SearchScreen
import ru.ivan.eremin.socialnetwork.presentation.splash.SplashScreen

@ExperimentalMaterialApi
@Composable
fun Navigation(navController: NavHostController, scaffoldState:ScaffoldState, scope: CoroutineScope) {
    NavHost(
        navController = navController,
        startDestination = Screen.SplashScreen.route,
        modifier = Modifier.fillMaxSize()
    ) {
        composable(Screen.SplashScreen.route) {
            SplashScreen(navController = navController, scaffoldState = scaffoldState)
        }
        composable(Screen.LoginScreen.route) {
            LoginScreen(navController = navController, scaffoldState = scaffoldState)
        }
        composable(Screen.RegisterScreen.route) {
            RegisterScreen(navController = navController, scaffoldState = scaffoldState)
        }
        composable(Screen.MainFeedScreen.route) {
            MainFeedScreen(navController = navController)
        }
        composable(Screen.ChatScreen.route) {
            ChatScreen(navController = navController)
        }
        composable(Screen.ActivityScreen.route) {
            ActivityScreen(navController = navController)
        }
        composable(Screen.ProfileScreen.route) {
            ProfileScreen(navController = navController)
        }
        composable(Screen.EditProfileScreen.route) {
            EditProfileScreen(navController = navController)
        }
        composable(Screen.CreatePostScreen.route) {
            CreatePostScreen(navController = navController)
        }
        composable(Screen.SearchScreen.route) {
            SearchScreen(navController = navController)
        }
        composable(Screen.PostDetailScreen.route) {
            PostDetailScreen(
                navController = navController,
                post = Post(
                    postId = "ddada",
                    username = "Ivan Eremin",
                    imageUrl = "",
                    profilePictureUrl = "",
                    description = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed\n" +
                            "diam nonumy eirmod tempor invidunt ut labore et dolore \n" +
                            "magna aliquyam erat, sed diam voluptua Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed\\n\" +\n" +
                            "                    \"diam nonumy eirmod tempor invidunt ut labore et dolore \\n\" +\n" +
                            "                    \"magna aliquyam erat, sed diam voluptua",
                    likeCount = 17,
                    commentCount = 7
                )
            )
        }
        composable(Screen.PersonListScreen.route) {
            PersonListScreen(navController = navController)
        }
    }
}