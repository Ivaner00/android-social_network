package ru.ivan.eremin.socialnetwork.models.post

data class Post(
    val postId: String,
    val username: String,
    val imageUrl: String,
    val profilePictureUrl: String,
    val description: String,
    val likeCount: Int,
    val commentCount: Int
)
