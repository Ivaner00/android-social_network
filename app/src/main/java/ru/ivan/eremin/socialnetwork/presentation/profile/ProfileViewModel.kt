package ru.ivan.eremin.socialnetwork.presentation.profile

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import ru.ivan.eremin.socialnetwork.domain.provider.user.UserProvider
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val userProvider: UserProvider
):ViewModel(){
    private val _toolbarState = mutableStateOf(ProfileToolbarState())
    val toolbarState: State<ProfileToolbarState> = _toolbarState

    private val _profileState = mutableStateOf(ProfileState())
    val profileState: State<ProfileState> = _profileState

    fun setExpandedRatio(ratio: Float) {
        _toolbarState.value = _toolbarState.value.copy(expandedRatio = ratio)
        println("UPDATING TOOLBAR STATE TO $toolbarState")
    }

    fun setToolbarOffsetY(value: Float) {
        _toolbarState.value = _toolbarState.value.copy(toolbarOffsetY = value)
        println("UPDATING TOOLBAR STATE TO $toolbarState")
    }

    init {
        viewModelScope.launch {
            userProvider.getUserProfile()
                .onSuccess {
                    _profileState.value = _profileState.value.copy(
                        profile = it
                    )
                }.onFailure {
                    _profileState.value = _profileState.value.copy(
                        exception = it
                    )
                }
            userProvider.getPostsForProfile()
                .onSuccess {
                    _profileState.value = _profileState.value.copy(
                        posts = it
                    )
                }.onFailure {
                    _profileState.value = _profileState.value.copy(
                        exception = it
                    )
                }
        }
    }
}