package ru.ivan.eremin.socialnetwork.data.repository.user

import ru.ivan.eremin.socialnetwork.models.request.user.LoginRequest
import ru.ivan.eremin.socialnetwork.models.request.user.UserAccountRequest
import ru.ivan.eremin.socialnetwork.models.response.user.*

interface UserApi {
    suspend fun createUser(user: UserAccountRequest): Result<RegisterResponse>
    suspend fun loginUser(login: LoginRequest): Result<AuthResponse>
    suspend fun checkAuth(userId: String): Result<CheckAuthResponse>
    suspend fun getUserProfile(userId: String): Result<ProfileResponse>
    suspend fun getPostsForProfile(userId: String): Result<List<PostResponse>>
}