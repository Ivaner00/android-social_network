package ru.ivan.eremin.socialnetwork.presentation.components

import androidx.compose.material.ScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@Composable
fun SnackBar(
    scaffoldState: ScaffoldState,
    dispatcher: CoroutineDispatcher = Dispatchers.Main,
    message: String
){
    LaunchedEffect(message) {
        withContext(dispatcher) {
            scaffoldState.snackbarHostState
                .showSnackbar(message = message)
        }
    }
}