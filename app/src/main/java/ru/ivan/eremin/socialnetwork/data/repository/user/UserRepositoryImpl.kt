package ru.ivan.eremin.socialnetwork.data.repository.user

import ru.ivan.eremin.socialnetwork.data.core.Mapper
import ru.ivan.eremin.socialnetwork.data.database.post.PostDao
import ru.ivan.eremin.socialnetwork.data.database.post.PostEntity
import ru.ivan.eremin.socialnetwork.data.database.profile.ProfileDao
import ru.ivan.eremin.socialnetwork.data.database.profile.ProfileEntity
import ru.ivan.eremin.socialnetwork.data.httpclient.NetworkHandler
import ru.ivan.eremin.socialnetwork.data.repository.sharedDataRepository.SharedDataRepository
import ru.ivan.eremin.socialnetwork.domain.exeption.InternalException
import ru.ivan.eremin.socialnetwork.domain.provider.user.UserRepository
import ru.ivan.eremin.socialnetwork.models.request.user.LoginRequest
import ru.ivan.eremin.socialnetwork.models.request.user.UserAccountRequest
import ru.ivan.eremin.socialnetwork.models.response.user.BaseResponse
import ru.ivan.eremin.socialnetwork.models.response.user.PostResponse
import ru.ivan.eremin.socialnetwork.models.response.user.ProfileResponse
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
    private val sharedDataRepository: SharedDataRepository,
    private val profileDao: ProfileDao,
    private val postDao: PostDao,
    private val networkHandler: NetworkHandler,
    private val api: UserApi
) : UserRepository {
    override suspend fun createUser(user: UserAccountRequest): Result<BaseResponse> {
        return api.createUser(user)
            .map {
                sharedDataRepository.saveUserId(it.userId ?: "")
                val response = BaseResponse()
                response.successful = it.successful
                response.message = it.message
                response
            }
    }

    override suspend fun loginUser(login: LoginRequest): Result<BaseResponse> {
        return api.loginUser(login = login)
            .map {
                sharedDataRepository.saveAccessToken(it.token ?: "")
                sharedDataRepository.saveUserId(it.userId ?: "")
                val response = BaseResponse()
                response.message = it.message
                response.successful = it.successful
                response
            }
    }

    override suspend fun getUserProfile(): Result<ProfileResponse> {
        val userId = sharedDataRepository.getUserId()
        return if (networkHandler.isConnected) {
            api.getUserProfile(userId)
                .map {
                    val mapper = ProfileEntityMap()
                    mapper.map(it)?.let {
                        profileDao
                            .insert(
                                it
                            )
                    }
                    it
                }
                .onFailure {
                    val profile = profileDao.getProfileById(userId = userId)
                    return if (profile != null) {
                        val mapper = ProfileEntityMap()
                        mapper.reverseMap(profile)?.let { response ->
                            Result.success(
                                response
                            )
                        } ?: Result.failure(it)
                    } else {
                        Result.failure(it)
                    }
                }
        } else {
            val profile = profileDao.getProfileById(userId = userId)
            return if (profile != null) {
                val mapper = ProfileEntityMap()
                mapper.reverseMap(profile)?.let {
                    Result.success(
                        it
                    )
                } ?: Result.failure(InternalException.NetworkConnectionException)
            } else {
                Result.failure(InternalException.NetworkConnectionException)
            }
        }
    }

    override suspend fun getPostsForProfile(): Result<List<PostResponse>> {
        val userId = sharedDataRepository.getUserId()
        return if (networkHandler.isConnected) {
            api.getPostsForProfile(userId)
                .map {
                    val mapper = PostEntityMap(userId)
                    mapper.map(it)?.let {
                        postDao.insert(it)
                    }
                    it
                }
                .onFailure {
                    val posts = postDao.getPostsForProfile(userId)
                    return if (!posts.isNullOrEmpty()) {
                        val mapper = PostEntityMap(userId)
                        mapper.reverseMap(posts)?.let { response ->
                            Result.success(response)
                        } ?: Result.failure(it)
                    } else {
                        Result.failure(it)
                    }
                }
        } else {
            val posts = postDao.getPostsForProfile(userId = userId)
            return if (!posts.isNullOrEmpty()) {
                val mapper = PostEntityMap(userId)
                mapper.reverseMap(posts)?.let {
                    Result.success(
                        it
                    )
                } ?: Result.failure(InternalException.NetworkConnectionException)
            } else {
                Result.failure(InternalException.NetworkConnectionException)
            }
        }
    }


    override suspend fun checkAuth(): Result<Boolean> {
        val userId = sharedDataRepository.getUserId()
        if (userId == "") {
            return Result.success(false)
        }
        return api.checkAuth(userId)
            .map {
                if (it.isCheck == false || it.isCheck == null) {
                    clearCache()
                }
                it.isCheck ?: false
            }
            .onFailure {
                clearCache()
                return Result.success(false)
            }
    }


    private suspend fun clearCache() {
        sharedDataRepository.clearAccessToken()
        sharedDataRepository.clearUserId()
        profileDao.clearAll()
    }
}

class PostEntityMap(private val userId: String) : Mapper<List<PostResponse>, List<PostEntity>>() {
    override fun mapImpl(items: List<PostResponse>): List<PostEntity> {
        return items.map { item ->
            PostEntity(
                item.postId,
                userId,
                item.username,
                item.imageUrl,
                item.profilePictureUrl,
                item.description,
                item.likeCount,
                item.commentCount
            )
        }
    }

    override fun reverseMapImpl(items: List<PostEntity>): List<PostResponse> {
        return items.map { item ->
            PostResponse(
                item.postId,
                item.username,
                item.imageUrl,
                item.profilePictureUrl,
                item.description,
                item.likeCount,
                item.commentCount
            )
        }
    }

}

class ProfileEntityMap : Mapper<ProfileResponse, ProfileEntity>() {
    override fun mapImpl(item: ProfileResponse): ProfileEntity {
        return ProfileEntity(
            item.userId,
            item.username,
            item.bio,
            item.followerCount,
            item.followingCount,
            item.postCount,
            item.profilePictureUrl,
            item.topSkillUrls,
            item.gitHubUrl,
            item.instagramUrl,
            item.linkedInUrl,
            item.isOwnProfile,
            item.isFollowing,
            item.description
        )
    }

    override fun reverseMapImpl(item: ProfileEntity): ProfileResponse {
        return ProfileResponse(
            item.userId,
            item.username,
            item.bio,
            item.description,
            item.followerCount,
            item.followingCount,
            item.postCount,
            item.profilePictureUrl,
            item.topSkillUrls,
            item.gitHubUrl,
            item.instagramUrl,
            item.linkedInUrl,
            item.isOwnProfile,
            item.isFollowing,
        )
    }

}