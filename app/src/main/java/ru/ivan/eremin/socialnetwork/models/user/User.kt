package ru.ivan.eremin.socialnetwork.models.user

data class User(
    val userId: String,
    val profilePictureUrl: String,
    val username: String,
    val description: String,
    val followerCount: Int,
    val followingCount: Int,
    val postCount: Int,
){
    constructor():this("","","","",0,0,0)
}
