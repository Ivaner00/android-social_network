package ru.ivan.eremin.socialnetwork.models.response.user

import kotlinx.serialization.Serializable

@Serializable
data class ProfileResponse(
    val userId: String,
    val username: String,
    val bio: String,
    val description: String,
    val followerCount: Int,
    val followingCount: Int,
    val postCount: Int,
    val profilePictureUrl: String,
    val topSkillUrls: List<String>,
    val gitHubUrl: String?,
    val instagramUrl: String?,
    val linkedInUrl: String?,
    val isOwnProfile: Boolean,
    val isFollowing: Boolean):BaseResponse()