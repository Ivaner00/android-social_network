package ru.ivan.eremin.socialnetwork.presentation.register

import android.util.Patterns
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import ru.ivan.eremin.socialnetwork.domain.provider.user.UserProvider
import ru.ivan.eremin.socialnetwork.util.Constants
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val userProvider: UserProvider
):ViewModel() {
    private val _state = mutableStateOf(RegisterState())
    val state: State<RegisterState> = _state

    fun onEvent(event: RegisterEvent){
        when(event){
            is RegisterEvent.EnteredUsername->{
                _state.value = _state.value.copy(
                    usernameText = event.value
                )
            }
            is RegisterEvent.EnteredEmail -> {
                _state.value = _state.value.copy(
                    emailText = event.value
                )
            }
            is RegisterEvent.EnteredPassword -> {
                _state.value = _state.value.copy(
                    passwordText = event.value
                )
            }
            is RegisterEvent.TogglePasswordVisibility -> {
                _state.value = _state.value.copy(
                    isPasswordVisible = !state.value.isPasswordVisible
                )
            }

            is RegisterEvent.Register -> {
                if(!validateUsername(state.value.usernameText)){
                    return
                }
                if(!validateEmail(state.value.emailText)){
                    return
                }
                if(!validatePassword(state.value.passwordText)){
                    return
                }
                viewModelScope.launch {
                    userProvider.createUser(
                        email = state.value.emailText,
                        username = state.value.usernameText,
                        password = state.value.passwordText
                    ).onSuccess {
                        _state.value = _state.value.copy(
                            registerResult = RegisterState.RegisterResult.Success(
                                it.successful ?: true,
                                it.message
                            )
                        )
                    }.onFailure {
                        _state.value = _state.value.copy(
                            registerResult = RegisterState.RegisterResult.Failed(it.localizedMessage)
                        )
                    }
                }
            }
        }
    }
    private fun validateUsername(username: String): Boolean {
        val trimmedUsername = username.trim()
        if(trimmedUsername.isBlank()) {
            _state.value = _state.value.copy(
                usernameError = RegisterState.UsernameError.FieldEmpty
            )
            return false
        }
        if(trimmedUsername.length < Constants.MIN_USERNAME_LENGTH) {
            _state.value = _state.value.copy(
                usernameError = RegisterState.UsernameError.InputTooShort
            )
            return false
        }
        _state.value = _state.value.copy(usernameError = null)
        return true
    }

    private fun validateEmail(email: String): Boolean {
        val trimmedEmail = email.trim()
        if(trimmedEmail.isBlank()) {
            _state.value = _state.value.copy(
                emailError = RegisterState.EmailError.FieldEmpty
            )
            return false
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _state.value = _state.value.copy(
                emailError = RegisterState.EmailError.InvalidEmail
            )
            return false
        }
        _state.value = _state.value.copy(emailError = null)
        return true
    }

    private fun validatePassword(password: String): Boolean {
        if(password.isBlank()) {
            _state.value = _state.value.copy(
                passwordError = RegisterState.PasswordError.FieldEmpty
            )
            return false
        }
        if(password.length < Constants.MIN_PASSWORD_LENGTH) {
            _state.value = _state.value.copy(
                passwordError = RegisterState.PasswordError.InputTooShort
            )
            return false
        }
        val capitalLettersInPassword = password.any { it.isUpperCase() }
        val numberInPassword = password.any { it.isDigit() }
        if(!capitalLettersInPassword || !numberInPassword) {
            _state.value = _state.value.copy(
                passwordError = RegisterState.PasswordError.InvalidPassword
            )
            return false
        }
        _state.value = _state.value.copy(passwordError = null)
        return true
    }
}