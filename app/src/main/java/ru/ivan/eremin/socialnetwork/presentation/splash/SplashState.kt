package ru.ivan.eremin.socialnetwork.presentation.splash

data class SplashState(
    val isCheckAuth: Boolean? = null,
    val errorMessage: String? = null
)