package ru.ivan.eremin.socialnetwork.data.core

abstract class Mapper<From, To> {

    fun map(item: From?): To? {
        return if (item == null) {
            null
        } else {
            mapImpl(item)
        }
    }

    fun reverseMap(item: To?): From? {
        return if (item == null) {
            null
        } else {
            reverseMapImpl(item)
        }
    }

    protected abstract fun mapImpl(item: From): To
    protected abstract fun reverseMapImpl(item: To): From
}