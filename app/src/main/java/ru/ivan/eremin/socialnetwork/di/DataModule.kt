package ru.ivan.eremin.socialnetwork.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ru.ivan.eremin.socialnetwork.data.repository.sharedDataRepository.SharedDataRepository
import ru.ivan.eremin.socialnetwork.data.repository.sharedDataRepository.SharedDataRepositoryImpl
import ru.ivan.eremin.socialnetwork.data.repository.user.UserApi
import ru.ivan.eremin.socialnetwork.data.repository.user.api.UserApiImpl

@Module
@InstallIn(SingletonComponent::class)
interface DataModule {
    @Binds
    fun bindUserApi(
        api: UserApiImpl
    ): UserApi

    @Binds
    fun bindSharedPreference(
        repo: SharedDataRepositoryImpl
    ): SharedDataRepository
}